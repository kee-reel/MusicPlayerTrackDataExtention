#include "plugin.h"

Plugin::Plugin() :
	PluginBase(this),
	m_dataExtention(new DataExtention(this))
{
	initPluginBase({
		{INTERFACE(IPlugin), this},
		{INTERFACE(IDataExtention), m_dataExtention},
        {INTERFACE(IMusicPlayerTrackDataExtention), m_dataExtention},
	});
}

Plugin::~Plugin()
{
}
