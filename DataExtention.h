#pragma once

#include <QObject>
#include <QDebug>
#include <QString>
#include <QAbstractItemModel>

#include "../../Interfaces/Middleware/idataextention.h"
#include "../../Interfaces/Utility/i_music_player_track_data_extention.h"

#include "../../Interfaces/Architecture/PluginBase/plugin_base.h"
#include "../../Interfaces/Middleware/DataExtentionBase/DataExtentionBase.h"

class DataExtention : public QObject, public IMusicPlayerTrackDataExtention
{
	Q_OBJECT
    Q_INTERFACES(IMusicPlayerTrackDataExtention IDataExtention)
    DATA_EXTENTION_BASE_DEFINITIONS(IMusicPlayerTrackDataExtention, IMusicPlayerTrackDataExtention, {"name", "path"})

public:
	DataExtention(QObject* parent) :
		QObject(parent)
	{
	}

	virtual ~DataExtention() = default;

signals:
	void modelChanged() override;

public:
    QString name() override
    {
        return "";
    }
	QString path() override
	{
		return "";
    }
};
